Deploy and test on Windows with:

~~~
kubectl delete service allinone-service ;  
kubectl delete deployment allinone-deployment ;  
cd python_app ;
docker build -t local/app-image -f Dockerfile_app . ; 
docker build -t local/counter-image -f Dockerfile_counter . ; 
cd .. ;
kubectl apply -f .\kubernetes\all-in-one.yaml ;
minikube service allinone-service
~~~