"""
"""
import logging
from fastapi import FastAPI
# from lib.simple_db import write_query, read_query, reset_database
from lib.complex_db import write_query, read_query, reset_database


app = FastAPI(title="App backend")

LOG_LEVEL = 0

logging.log(level=LOG_LEVEL, msg="Resetting and filling the database")
# reset_database(force=True, fill_with_data=True)
logging.log(level=LOG_LEVEL, msg="[DONE]")


@app.get("/")
def _index():
    return {"Hello": "World!"}

@app.get("/db_alive")
def _db_alive():
    try:
        r = read_query("SELECT 1")
        alive = True
    except Exception as e:
        r = e
        alive = False
    return {"alive": alive, "details": str(r)}


@app.get("/db_create")
def _db_create():
    r = write_query("CREATE TABLE users(id serial PRIMARY KEY, name VARCHAR (100) NOT NULL);")
    return {"action": "created the table", "result": r}

@app.get("/db_fill")
def _db_fill():
    r = write_query("INSERT INTO users VALUES (0, 'PVF'), (1, 'admin'), (2, 'random');")
    return {"action": "filled the table", "result": r}

@app.get("/db_reset")
def _reset_db():
    reset_database(force=True, fill_with_data=True)

@app.get("/users")
def _user_list():
    r = read_query("SELECT name from users")
    return {"users": [i[0] for i in r]}

@app.get("/counters")
def _user_list():
    r = read_query("SELECT ping_time from counter")
    return {"counter_pings": [i[0] for i in r]}

@app.get("/add_user")
def _add_user(name: str):
    write_query(f"INSERT INTO users(id, name) VALUES (10, '{name}');")
    return {"added": name}

@app.get("/user_exists")
def _user_exists(name: str):
    r = read_query(f"SELECT name from users WHERE name='{name}';")
    return {"exists": len(r)>0}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, port=30080)