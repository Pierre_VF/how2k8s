import os
import psycopg2

__port_str = os.environ.get("POSTGRES_PORT")
if __port_str is None:
    __port_str = "5432"
__POSTGRES_PORT = int(__port_str)
__POSTGRES_PASSWORD = os.environ.get("POSTGRES_PASSWORD")

def reset_database(force: bool = True, fill_with_data: bool = False):
    if force:
        try:
            write_query(
                [
                    "DROP TABLE users;",
                ]
            )
        except Exception as e:
            print(f"Table users does not exist - skipping drop ({e})")
    con = db_connection()
    con.close()
    if fill_with_data:
        write_query(
            [
                "CREATE TABLE users(id serial PRIMARY KEY, name VARCHAR (100) NOT NULL);",
                "INSERT INTO users VALUES (0, 'PVF'), (1, 'admin'), (2, 'random');",
            ]
        )

def db_connection():
    conn = psycopg2.connect(
        database="postgres",
        host="localhost",
        user="postgres",
        password=__POSTGRES_PASSWORD,
        port=__POSTGRES_PORT,
    )
    conn.autocommit = True
    return conn

def write_query(query: list[str] | str) -> None:
    con = db_connection()
    cursor = con.cursor()
    if isinstance(query, str):
        res = cursor.execute(query)
    else:
        res = [cursor.execute(q) for q in query]
    con.close()

def read_query(query: str):
    con = db_connection()
    cursor = con.cursor()
    cursor.execute(query)
    out = cursor.fetchall()
    con.close()
    return out
