"""
File containing a routine that fills the database with data on a regular basis
"""
from time import sleep
import logging
import datetime

# from lib.simple_db import write_query, read_query, reset_database
from lib.complex_db import write_query


LOG_LEVEL = 0
logging.log(level=LOG_LEVEL, msg="Starting up the routine")

try:
    r = write_query("CREATE TABLE counter(id serial PRIMARY KEY, ping_time VARCHAR (100) NOT NULL);")
except Exception as e:
    logging.log(level=LOG_LEVEL, msg=f"EXCEPTION: Failed to create the counter table ({e})")
except BaseException as e:
    logging.log(level=LOG_LEVEL, msg=f"BASE_EXCEPTION: Failed to create the counter table ({e})")


while True:
    t_now = datetime.datetime.now()
    logging.log(level=LOG_LEVEL, msg=f"Pinging the counter ({t_now})")
    query = f"INSERT INTO counter(ping_time) VALUES ('{t_now}')"
    write_query(query=query)
    sleep(30)